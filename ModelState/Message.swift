//
//  Message.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import Foundation.NSDate

public enum Message: Equatable {
    
    case todo(_Todo)
    case journal(_Journal)
    case tagging(_Tagging)
    
    public enum _Todo: Equatable {
        case add    (TodoItem)
        case delete (TodoItem)
        case check  (TodoItem)
        case uncheck(TodoItem)
        case change(_Change)

        case fork   (TodoItem, with: [Fork])
        
        case response(_Response)
        
        public enum _Change: Equatable {
            case due(to: Date?, for: TodoItem)
            case text(to: String, for: TodoItem)
        }
        
        public enum _Response: Equatable {
            case wasAdded    (TodoItem)
            case wasDeleted  (TodoItem)
            case wasChecked  (TodoItem)
            case wasUnchecked(TodoItem)
            case wasForked(TodoItem)
            case dueDateWasChanged(TodoItem)
            case textDateWasChanged(TodoItem)
        }
    }
    
    public enum _Journal: Equatable {
        case add(Entry)
        
        case response(_Response)
        
        public enum _Response: Equatable {
            case wasAdded(Entry)
        }
    }
    
    public enum _Tagging: Equatable {
        case add(Tag)
        case tag  (TodoItem, with: Tag)
        case untag(TodoItem, from: Tag)
        
        case response(_Response)
        
        public enum _Response: Equatable {
            case wasAdded(Tag)
            case wasTagged  (TodoItem, with: Tag)
            case wasUntagged(TodoItem, from: Tag)
        }
    }
}
