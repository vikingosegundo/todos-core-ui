//
//  Entry.swift
//  Todos
//
//  Created by Manuel Meyer on 13/09/2020.
//

import Foundation

public
struct Entry: Identifiable, Hashable, Codable, Equatable {
    public init(text:String, id: UUID = UUID(), creationDate: Date = Date()) {
        self.id = id
        self.text = text.trimmingCharacters(in: .whitespacesAndNewlines)
        self.creationDate = creationDate
    }
    public let id: UUID
    public let text: String
    public let creationDate: Date
}
