//
//  Fork.swift
//  Todos
//
//  Created by Manuel Meyer on 18.04.21.
//

import Foundation

public 
enum Fork: Equatable {
    case text(String)
    case due(Date?)
}
