//
//  Tag.swift
//  Todos
//
//  Created by Manuel Meyer on 14/09/2020.
//

import Foundation.NSUUID

public func change(_ tag: Tag, _ change:  Tag.Change...) -> Tag { tag.alter(change) }
public func change(_ tag: Tag, _ change: [Tag.Change]  ) -> Tag { tag.alter(change) }

public
struct Tag: Identifiable, SelectionItem, Codable, Equatable {
    
    public let id: UUID
    public let name: String
    public let tagged: [UUID]
    
    public enum Change {
        case by(_By)
        
        public enum _By {
            case adding  (TodoItem)
            case removing(TodoItem)
        }
    }
    
    public init(name: String) {
        self.init(name, [], UUID())
    }
    
    fileprivate func alter(_ changes: [Change] ) -> Tag { changes.reduce(self) { $0.alter($1) } }

    public var selectionTitle: String { name }
    
    private init(_ name: String, _ tagged: [UUID], _ id: UUID) {
        self.name = name.trimmingCharacters(in: .whitespacesAndNewlines)
        self.tagged = tagged
        self.id = id
    }
    
    private func alter(_ change:Change) -> Tag {
        switch change {
        case .by(  .adding(let tagable)): return Tag(name, tagged.filter { $0 != tagable.id } + [tagable.id], id)
        case .by(.removing(let tagable)): return Tag(name, tagged.filter { $0 != tagable.id }               , id)
        }
    }
}
