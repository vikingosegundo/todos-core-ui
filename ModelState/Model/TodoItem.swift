//
//  TodoItem.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import Foundation

public func change(_ item: TodoItem, _ changes: TodoItem.Change...) -> TodoItem { item.alter(changes) }
public func change(_ item: TodoItem, _ changes: [TodoItem.Change] ) -> TodoItem { item.alter(changes) }
public func dueDate(of item: TodoItem) -> Date? { item.dueDate }

public
struct TodoItem: Identifiable, Hashable, Codable {
    public enum Change: Equatable, Identifiable {
        public var id: String {
            switch self {
            case .by(.updating(.completed(let     b))): return "\(b)"
            case .by(.updating(.dueDate  (let     d))): return d != nil ? "\(d!)" : "\(Date())"
            case .by(.updating(.text     (let     t))): return t
            case .by(.updating(.fork     (let inner))): return inner.reduce("") { "\($0)\($1)" }
            }
        }
        
        case by(_By)
        public enum _By: Equatable {
            case updating(_Changing)
            
            public enum _Changing: Equatable {
                case text     (to: String)
                case completed(to: Bool  )
                case dueDate  (to: Date? )
                case fork     (  [Change])
            }
        }
    }
    
    public let text        : String
    public let completed   : Bool
    public let id          : UUID
    public let dueDate     : Date?
    public let alterDate   : Date?
    public let creationDate: Date

    public init(text: String) { self.init(UUID(), text, false, nil, Date(), nil) }
    
    fileprivate func alter(_ changes: [Change] ) -> TodoItem { changes.reduce(self) { $0.alter($1) } }
}

private
extension TodoItem{
    private init (_ id: UUID, _ text: String, _ completed: Bool,  _ dueDate: Date?, _ creationDate: Date, _ alterDate: Date?) {
        self.text         = text
        self.completed    = completed
        self.id           = id
        self.creationDate = creationDate
        self.dueDate      = dueDate
        self.alterDate    = alterDate
    }
    
    private func alter(_ change: Change) -> TodoItem {
        let changedItem = item(with: change)
        switch change {
        case .by(.updating(.fork(_))):
            return changedItem
        default:
            return bruttoEqual(lhs: changedItem, rhs: self) ? self : changedItem
        }
    }

    private func item(with change: Change) -> TodoItem {
        let altered = Date()
        switch change {
        case .by(.updating(.text     (let text     ))): return TodoItem(id    , text, completed, dueDate, creationDate, altered  )
        case .by(.updating(.completed(let completed))): return TodoItem(id    , text, completed, dueDate, creationDate, altered  )
        case .by(.updating(.dueDate  (let dueDate  ))): return TodoItem(id    , text, completed, dueDate, creationDate, altered  )
        case .by(.updating(.fork     (let changes  ))): return TodoItem(UUID(), text, completed, dueDate, creationDate, alterDate).alter(changes)
        }
    }
}

fileprivate func bruttoEqual(lhs: TodoItem, rhs: TodoItem) -> Bool {
    lhs.text      == rhs.text
 && lhs.dueDate   == rhs.dueDate
 && lhs.completed == rhs.completed
}

