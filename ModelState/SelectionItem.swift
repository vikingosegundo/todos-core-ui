//
//  SelectionItem.swift
//  ModelState
//
//  Created by Manuel Meyer on 10.05.21.
//


public protocol SelectionItem: Identifiable {
    var selectionTitle: String { get }
}

