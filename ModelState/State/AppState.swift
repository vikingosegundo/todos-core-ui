//
//  AppState.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

public func items  (in s: AppState) -> [ TodoItem ] { s.items   }
public func tags   (in s: AppState) -> [ Tag      ] { s.tags    }
public func entries(in s: AppState) -> [ Entry    ] { s.entries }

public func change(_ state: AppState, _ change:   AppState.Change...) -> AppState { state.alter(change) }
public func change(_ state: AppState, _ change: [ AppState.Change ] ) -> AppState { state.alter(change) }

public struct AppState: Codable {
    public enum Change: Equatable {
        case by(_By)
        
        public enum _By: Equatable {
            case adding   (_Add    )
            case updating (_Update )
            case removing (_Remove )
            case replacing(_Replace)
        }
        
        public enum _Add: Equatable {
            case item (TodoItem)
            case entry(Entry   )
            case tag  (Tag     )
        }
        public enum _Update : Equatable { case item(with: TodoItem) }
        public enum _Replace: Equatable { case tags(with: [ Tag ] ) }
        public enum _Remove : Equatable { case item(      TodoItem) }
    }
    
    public init() { self.init([], [], []) }
    
    fileprivate func alter(_ changes: [ Change ] ) -> AppState { changes.reduce(self) { $0.alter($1) } }

    public let items: [TodoItem]
    fileprivate let entries: [Entry]
    fileprivate let tags: [Tag]
}

private extension AppState {
    private init(_ items: [TodoItem], _ entries: [Entry], _ tags: [Tag]) {
        self.items = items
        self.entries = entries
        self.tags = tags
    }
    
    private func alter(_ change:Change) -> AppState {
        switch change {
        case .by(.adding   (let msg)): return add    (msg)
        case .by(.updating (let msg)): return update (msg)
        case .by(.removing (let msg)): return remove (msg)
        case .by(.replacing(let msg)): return replace(msg)
        }
    }
    
    private func add(_ change: Change._Add) -> AppState {
        switch change {
        case  .item(let  item): return AppState( items + [item], entries          , tags         )
        case .entry(let entry): return AppState( items         , entries + [entry], tags         )
        case   .tag(let   tag): return AppState( items         , entries          , tags + [tag] )
        }
    }
    
    private func update(_ change: Change._Update) -> AppState {
        switch change { case .item(let item): return AppState( items.filter{ $0.id != item.id } + [item], entries, tags ) }
    }
    
    private func remove(_ c: Change._Remove) -> AppState {
        switch c {
        case .item(let item): return AppState( items.filter{ $0.id != item.id }, entries, tags.map( { change($0, .by(.removing(item))) } ) )
        }
    }
    
    private func replace(_ change: Change._Replace) -> AppState {
        switch change { case .tags(let tags): return AppState(items, entries, tags) }
    }
}
