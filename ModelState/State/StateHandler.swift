//
//  StateHandler.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import Foundation.NSFileManager

typealias Change   = ( AppState.Change... ) -> ()
typealias Reset    = (                    ) -> ()
typealias Access   = (                    ) -> AppState
typealias Callback = ( @escaping () -> () ) -> ()

typealias StateStore = ( change: Change, reset: Reset, state: Access, updated: Callback )

func createStore(pathInDocuments: String = "state.json", fileManager: FileManager = FileManager.default) -> StateStore
{
    var state = readAppSate(from: pathInDocuments, fileManager: fileManager) { didSet { callbacks.forEach { $0() } } }
    var callbacks: [() -> ()] = []
    return (
         change: { state = state.alter($0); write(state: state, to: pathInDocuments, fileManager: fileManager) },
          reset: { state = AppState()     ; write(state: state, to: pathInDocuments, fileManager: fileManager) },
          state: { state                  },
        updated: { callbacks.append($0)   }
    )
}

fileprivate func write(state: AppState, to pathInDocuments: String, fileManager: FileManager ) {
    do {
        let encoder = JSONEncoder()
        #if DEBUG
        encoder.outputFormatting = .prettyPrinted
        #endif
        let data = try encoder.encode(state)
        try data.write(to: fileURL(pathInDocuments: pathInDocuments, fileManager: fileManager))
    } catch {
        print(error)
    }
}

fileprivate func readAppSate(from pathInDocuments: String, fileManager: FileManager) -> AppState {
    do {
        return try JSONDecoder().decode(AppState.self, from: try Data(contentsOf: fileURL(pathInDocuments: pathInDocuments, fileManager: fileManager)))
    } catch {
        print(error)
    }
    return AppState()
}

fileprivate func fileURL(pathInDocuments: String, fileManager: FileManager) throws -> URL {
    try fileManager
        .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        .appendingPathComponent(pathInDocuments)
}
