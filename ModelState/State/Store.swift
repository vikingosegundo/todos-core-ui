//
//  StateHandler.swift
//  Todos
//
//  Created by Manuel Meyer on 12/04/2021.
//

public typealias Access   = (                    ) -> AppState
public typealias Change   = ( AppState.Change... ) -> ()
public typealias Reset    = (                    ) -> ()
public typealias Callback = ( @escaping () -> () ) -> ()
public typealias Destroy =  (                    ) -> ()

public typealias Store = ( state: Access, change: Change, reset: Reset, updated: Callback, destroy: Destroy )

public func state   (of store: Store                             ) -> AppState { store.state()                    }
public func change  (_  store: Store, _ cs:   AppState.Change... )             { cs.forEach { store.change($0)  } }
public func change  (_  store: Store, _ cs: [ AppState.Change ]  )             { cs.forEach { change(store, $0) } }
public func reset   (_  store: Store                             )             { store.reset();                   }
public func destroy (_  store: inout Store!                      )             { store.destroy(); store = nil     }
