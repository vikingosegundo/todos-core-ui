//
//  ViewState.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import SwiftUI


public func items  (in s: ViewState) -> [ TodoItem ] { s.itemlist  }
public func entries(in s: ViewState) -> [ Entry    ] { s.entrylist }
public func tags   (in s: ViewState) -> [ Tag      ] { s.taglist   }
public func resetDeletedItem(in s: ViewState) { s.deleted = nil  }
public func deletedItem(in s: ViewState) -> TodoItem? { s.deleted  }

final
public class ViewState: ObservableObject  {
    
    @Published fileprivate var itemlist : [ TodoItem ] = []
    @Published fileprivate var entrylist: [ Entry    ] = []
    @Published fileprivate var taglist  : [ Tag      ] = []
    @Published fileprivate var deleted  :   TodoItem?
    
    public init(store: Store, dismissToastAfter: TimeInterval = 2) {
        self.dismissToastAfter = dismissToastAfter
        store.updated { [self] in
            process( state(of: store) )
        }
        process( state(of: store) )
    }

    private let dismissToastAfter: TimeInterval

    public func handle(msg: Message) {
        if case .todo(.response(.wasDeleted(item: let item))) = msg {
            deleted = item
            DispatchQueue.main.asyncAfter(deadline: .now() + dismissToastAfter) {
                self.deleted = nil
            }
        }
    }
    
    private func process(_ appState: AppState) {
        itemlist  = items  (in: appState).sorted(on: \.text,         by: <)
        entrylist = entries(in: appState).sorted(on: \.creationDate, by: <)
        taglist   = tags   (in: appState).sorted(on: \.name,         by: <)
    }
}
