//
//  Collection+KeyPathSorting.swift
//  Todos
//
//  Created by Manuel Meyer on 19/09/2020.
//

import Foundation


public func last <Element>(of array: [Element]) -> Element? { array.last  }
public func first<Element>(of array: [Element]) -> Element? { array.first }

public func createArray<Element>(by isIncluded: (Element) throws -> Bool, from array:[Element] ) -> [Element] {
    array.filter {
        do {
            return try isIncluded($0)
        } catch { return false }
    }
}

extension Collection {
    
    public func sorted<Value: Comparable>(on property: KeyPath<Element, Value>, by areInIncreasingOrder: (Value, Value) -> Bool) -> [Element] {
        sorted { currentElement, nextElement in
            areInIncreasingOrder(currentElement[keyPath: property], nextElement[keyPath: property])
        }
    }
}

public
extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding: .day, value: -1, to: self)! }
    var  dayAfter: Date { cal.date(byAdding: .day, value:  1, to: self)! }
    var  midnight: Date { cal.date(bySettingHour:  0, minute: 0, second: 0, of: self)!}
    var      noon: Date { cal.date(bySettingHour: 12, minute: 0, second: 0, of: self)!}

    private var cal: Calendar { Calendar.current }
}

public func humanDate(_ date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "hMdmY", options: 0, locale: Locale.current)
    return formatter.string(from: date)
}


