//
//  ContentView.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import SwiftUI
import ModelState


public struct ContentView: View {
    
    public init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    @ObservedObject
    private(set) var viewState: ViewState
    private let rootHandler: (Message) -> ()
    
    public var body: some View {
        ZStack(alignment: .top) {
            VStack {
                TabView {
                    TodoView(viewState: viewState, rootHandler: rootHandler)
                        .tabItem { Image(systemName: "list.dash")
                            Text("todos")
                        }
                    TagListView(viewState: viewState, rootHandler: rootHandler)
                        .tabItem {
                            Image(systemName: "tag")
                            Text("tags")
                        }
                    JournalView(viewState: viewState, rootHandler: rootHandler)
                        .tabItem {
                            Image(systemName: "pencil")
                            Text("journal")
                        }
                }
            }
            VStack {
                if deletedItem(in: viewState) != nil  {
                    HStack {
                        Spacer()
                        Text("\(deletedItem(in: viewState)!.text) was deleted")
                        Spacer()
                        Button {
                            rootHandler(.todo(.add(deletedItem(in: viewState)!)))
                            resetDeletedItem(in: viewState)
                        } label: {
                            VStack {
                                Image(systemName: "arrow.uturn.left.circle")
                                Text("undo")
                            }
                        }
                    }.padding()
                    .background(Color.orange).padding()
                }
            }
        }
    }
}


