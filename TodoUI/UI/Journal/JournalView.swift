//
//  JournalView.swift
//  Todos
//
//  Created by Manuel Meyer on 14/09/2020.
//

import SwiftUI
import ModelState

public
struct JournalView: View {
    public init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    @State
    private var text:String = ""
    
    public var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(Array(entries(in: viewState))) { item in
                        Text(item.text)
                    }
                }.listStyle(GroupedListStyle())
                HStack {
                    TextField("enter entry", text: $text)
                    Button(action: { rootHandler(.journal(.add(Entry(text:text)))); text = ""}) { Text("add") }
                        .disabled(text.count < 1)
                }.padding()
            }.navigationTitle("Journal")
        }
    }
}
