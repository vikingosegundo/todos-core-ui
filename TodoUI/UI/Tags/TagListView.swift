//
//  TagListView.swift
//  Todos
//
//  Created by Manuel Meyer on 16/09/2020.
//

import SwiftUI
import ModelState

struct TagListView: View {
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    @State
    private var text:String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(Array(tags(in:viewState))) { tag in
                        NavigationLink(destination: TagView(tag: tag, viewState: viewState, rootHandler:rootHandler).navigationTitle("Tagged \(tag.name)")) {
                            TagPillView(tag: tag, tagColor: .gray, textColor: .white, font: .subheadline, radius: 5, padding: 5)
                        }
                    }
                }
                .listStyle(GroupedListStyle())
                HStack {
                    TextField("enter tag", text: $text)
                    Button(action: { rootHandler(.tagging(.add(Tag(name: text)))); text = ""}) { Text("add") }
                        .disabled(text.count < 1)
                }.padding()
            }.navigationTitle("Tags")
        }
    }
}
