//
//  TagPillView.swift
//  Todos
//
//  Created by Manuel Meyer on 17/09/2020.
//

import SwiftUI
import ModelState

struct TagPillView: View {
    private let tag: Tag
    private let tagColor: Color
    private let textColor: Color
    private let font: Font
    private let radius: CGFloat
    private let padding: CGFloat
    
    init(tag: Tag, tagColor: Color = .purple, textColor: Color = .white, font: Font = .footnote, radius: CGFloat = 5, padding: CGFloat = 2) {
        self.tag = tag
        self.tagColor = tagColor
        self.textColor = textColor
        self.font = font
        self.radius = radius
        self.padding = padding
    }
    
    var body: some View {
        Text(tag.name)
            .font(font)
            .padding(.init(top: padding, leading: padding, bottom: padding, trailing: padding))
            .foregroundColor(textColor)
            .background(tagColor)
            .cornerRadius(radius)
            .overlay(
                RoundedRectangle(cornerRadius: radius)
                    .stroke(tagColor, lineWidth: 2)
            )
    }
}
