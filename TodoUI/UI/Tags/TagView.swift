//
//  TagView.swift
//  Todos
//
//  Created by Manuel Meyer on 17/09/2020.
//

import SwiftUI
import ModelState

struct TagView: View {
    init(tag: Tag, viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.tag = tag
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    
    let tag: Tag
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    
    var body: some View {
        List(tag.tagged, id:\.self) { id in
            Section(header: Text("")){
                ForEach(items(in: viewState).filter({ $0.id == id })) {
                    TodoItemCell(item: $0, viewState: viewState, rootHandler: rootHandler)
                }
            }
        }
        .listStyle(GroupedListStyle())
        .navigationBarTitle(Text(tag.name), displayMode: .inline)
    }
}
