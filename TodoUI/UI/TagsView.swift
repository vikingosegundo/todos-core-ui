//
//  TagsView.swift
//  Todos
//
//  Created by Manuel Meyer on 16/09/2020.
//

import SwiftUI

struct TagView: View {
    init(tag: Tag, viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.tag = tag
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    
    let tag: Tag
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    
    var body: some View {
        List(tag.tagged, id:\.self) { id in
            Section(header: Text("")){
                ForEach(viewState.items.filter({ $0.id == id })) {
                    TodoItemCell(item: $0, viewState: viewState, rootHandler: rootHandler)
                }
            }
        }
        .listStyle(GroupedListStyle())
        .navigationBarTitle(Text("Tagged \"\(tag.name)\""), displayMode: .automatic )
    }
}

struct TagListView: View {
    
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
    @State
    private var text:String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(Array(viewState.tags)) { tag in
                        NavigationLink(destination: TagView(tag: tag, viewState: viewState, rootHandler:rootHandler).navigationTitle("Tagged \(tag.name)")) {
                            TagPillView(tag: tag, tagColor: .gray, textColor: .white, font: .subheadline, radius: 5, padding: 5)
                        }
                    }
                }
                .listStyle(GroupedListStyle())
                HStack {
                    TextField("enter tag", text: $text)
                    Button(action: { rootHandler(.tagging(.tag(.add(Tag(name: text))))); text = ""}) { Text("add") }
                        .disabled(text.count < 1)
                }.padding()
            }.navigationTitle("Tags")
        }
    }
}
