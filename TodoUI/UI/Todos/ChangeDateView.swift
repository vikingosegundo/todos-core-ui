//
//  ChangeDateView.swift
//  Todos
//
//  Created by Manuel Meyer on 18.04.21.
//

import SwiftUI
import ModelState

struct ChangeDateView: View {
    @State var entry: Date
    let entered: (Date?) -> ()
    
    var body: some View {
        VStack {
            DatePicker("Due Date", selection: $entry)
            HStack {
                Button { entered(entry) } label: { Text("OK") }
                Button { entered(nil) } label: { Text("Delete") }
            }
        }.padding()
    }
}
