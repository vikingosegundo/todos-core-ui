//
//  ChangeTextView.swift
//  Todos
//
//  Created by Manuel Meyer on 18.04.21.
//

import SwiftUI
import ModelState

struct ChangeTextView: View {

    @State var entry: String
    let entered: (String) -> ()
    
    var body: some View {
        VStack {
            TextField("text", text: $entry)
            Button { entered(entry) } label: { Text("OK") }
        }.padding()
    }
}
