//
//  TodoItemCell.swift
//  Todos
//
//  Created by Manuel Meyer on 17/09/2020.
//

import SwiftUI
import ModelState

struct TodoItemCell: View {
    
    init(item: TodoItem, viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.item = item
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    private let item: TodoItem
    @ObservedObject private var viewState: ViewState
    private let rootHandler: (Message) -> ()
    
    @Environment(\.colorScheme) var colorScheme

    @State private var selectedTags: [Tag] = []
    @State private var tagItem: TodoItem?
    @State private var presentTextSheet: Bool = false
    @State private var presentDateSheet: Bool = false
    @State private var isForking: Bool = false
    
    var body: some View {
        VStack {
            HStack {
                Text(item.text)
                    .foregroundColor(item.completed ? .gray : colorScheme == .light ? .black: .gray)
                    .strikethrough(item.completed)
                Spacer()
            }
            HStack {
                ForEach(tags(in:viewState).filter({ tag in tag.tagged.contains(where: { $0 == item.id }) })) { TagPillView(tag: $0, tagColor: .gray, textColor: .white) }
                Spacer()
                Text(item.dueDate != nil ? humanDate(item.dueDate!) : "")
            }
        }
        .contextMenu {
            Button("\(item.dueDate == nil ? "add" : "change") due date...")
                { isForking = false; presentDateSheet = true }
            Button("change text...")
                { isForking = false; presentTextSheet = true }
            Button(item.completed ? "uncheck" : "check")
                { rootHandler( .todo(item.completed ? .uncheck(item) : .check(item)) ) }
            Button("delete")
                { rootHandler( .todo(.delete(item)) )                                  }
            Button("tag...")
                { selectedTags = tags(in:viewState).filter{ $0.tagged.filter { $0 == item.id }.count > 0 }; tagItem = item }
            Menu("fork...") {
                Button("unchanged")
                    { rootHandler( .todo(.fork(item, with: [])) ) }
                Button("with changed text...")
                    { isForking = true; presentTextSheet = true }
                Button("with due date...")
                    { isForking = true; presentDateSheet = true }
            }
        }
        .sheet(item:$tagItem, onDismiss: { selectedTags = [] }) { item in
            SelectionView(items: tags(in:viewState), selected: $selectedTags) {
                switch $0 {
                case   .selected(let tag): rootHandler( .tagging(  .tag(item, with: tag)) )
                case .unselected(let tag): rootHandler( .tagging(.untag(item, from: tag)) )
                }
            }
        }
        .sheet(isPresented: $presentDateSheet) {
            ChangeDateView(entry: item.dueDate ?? Date()) {
                isForking
                ? rootHandler( .todo(  .fork(item, with: [.due($0)])) )
                : rootHandler( .todo(.change(.due(to:$0, for: item))) )
                presentDateSheet = false
            }
        }
        .sheet(isPresented: $presentTextSheet) {
            ChangeTextView(entry: item.text) {
                isForking
                ? rootHandler( .todo(  .fork( item, with: [.text($0)])) )
                : rootHandler( .todo(.change(.text(to: $0, for: item))) )
                presentTextSheet = false
            }
        }
    }
}
