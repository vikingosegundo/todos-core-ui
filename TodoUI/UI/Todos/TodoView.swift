//
//  TodoView.swift
//  Todos
//
//  Created by Manuel Meyer on 14/09/2020.
//

import SwiftUI
import ModelState

struct TodoView: View {
    
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    @ObservedObject private var viewState: ViewState
    private let rootHandler: (Message) -> ()
    @State private var text:String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    Section(header: Text("")) {
                        ForEach(Array(items(in: viewState).reversed())) { item in
                            TodoItemCell(item: item, viewState: viewState, rootHandler: rootHandler)
                        }
                        .onDelete {
                            if let idx = $0.first { rootHandler(.todo(.delete(items(in: viewState).reversed()[idx]))) }
                        }
                    }
                }
                .listStyle(GroupedListStyle())
                HStack {
                    TextField("enter todo", text: $text)
                    Button { rootHandler(.todo(.add(TodoItem(text:text)))); text = ""} label: { Text("add") }
                        .disabled(text.count < 1)
                }.padding()
            }.navigationTitle("Todos")
        }
    }
}
