//
//  SelectionView.swift
//  Todos
//
//  Created by Manuel Meyer on 17/09/2020.
//

import SwiftUI
import ModelState

public struct SelectionView<Element: SelectionItem>: View {
    
    public enum Selection {
        case selected  (Element)
        case unselected(Element)
    }
    
    public let items: [Element]
    @Binding var selected: [Element]
    public let selectedItem: (Selection) -> ()
    
    public var body: some View {
        ZStack {
            List {
                ForEach(items) { item in
                    HStack {
                        Text(item.selectionTitle)
                        Spacer()
                        Image(systemName: selected.filter({ $0.id == item.id }).count > 0
                                ? "checkmark.circle.fill"
                                : "checkmark.circle"
                        )
                    }
                    .onTapGesture {
                        if self.selected.filter ({ $0.id == item.id }).first != nil {
                            self.selected = selected.filter { $0.id != item.id }
                            selectedItem(.unselected(item))
                        } else {
                            self.selected = selected.filter { $0.id != item.id } + [item]
                            selectedItem(.selected(item))
                        }
                    }
                }
            }
        }
    }
}
