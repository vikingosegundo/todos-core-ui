//
//  TodosApp.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import SwiftUI
import ModelState
import TodoUI

@main
final
class TodosApp: App {
    
    private let store: Store = createDiskStore()
    private lazy var viewState: ViewState = ViewState(store: store)
    private lazy var rootHandler: (Message) -> ()
        = createAppDomain(store: store, receivers: [viewState.handle(msg:)], rootHandler: { self.rootHandler($0) })

    var body: some Scene { WindowGroup { ContentView( viewState: viewState, rootHandler: rootHandler ) } }
}
