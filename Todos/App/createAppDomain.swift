//
//  createAppCore.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import ModelState

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

func createAppDomain(
          store: Store,
      receivers: [Input],
    rootHandler: @escaping Output) -> Input
{
    store.updated {
        #if targetEnvironment(simulator)
        print("\(state(of: store))")
        #endif
    }
    
    let features: [Input] = [
        createTodoListFeature  (store: store, output: rootHandler),
        createTaggingFeature   (store: store, output: rootHandler),
        createJournalingFeature(store: store, output: rootHandler)
    ]
    
    return { msg in
        (receivers + features).forEach { $0(msg) }
    }
}
