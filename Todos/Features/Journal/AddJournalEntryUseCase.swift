//
//  AddJournalEntryUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 14/09/2020.
//

import ModelState

func request(_ request: AddJournalEntryUseCase.Request, from adder: AddJournalEntryUseCase) { adder.request(request) }

struct AddJournalEntryUseCase: UseCase {
    enum Request  { case add     (entry: Entry) }
    enum Response {
        case entry(Entry, _Entry)
        enum _Entry {
            case wasAdded
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .add(entry: let entry):
            change (store, .by(.adding(.entry(entry))) )
            respond(.entry(entry, .wasAdded)           )
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let store: Store
    private let respond: (Response) -> ()
}

extension AddJournalEntryUseCase.Response: Equatable {}
