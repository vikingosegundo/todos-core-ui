//
//  JournalingFeature.swift
//  Todos
//
//  Created by Manuel Meyer on 13/09/2020.
//
import ModelState

func createJournalingFeature (store: Store, output: @escaping Output) -> Input {
    let entryAdder = AddJournalEntryUseCase(store: store) { response in
        switch response {
        case .entry(let entry, .wasAdded): output( .journal(.response(.wasAdded(entry))) )
        }
    }
    return { msg in
        if case .journal(.add(let entry)) = msg { request( .add(entry: entry), from: entryAdder) }
    }
}
