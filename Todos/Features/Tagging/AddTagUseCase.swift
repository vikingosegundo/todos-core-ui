//
//  AddTagUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 15/09/2020.
//

import ModelState

func request(_ request: AddTagUseCase.Request, from adder: AddTagUseCase) { adder.request(request) }

struct AddTagUseCase: UseCase {
    enum Request  { case add     (tag: Tag) }
    enum Response {
        case tag(Tag,_Tag)
        enum _Tag {
            case wasAdded
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .add(tag: let t):
            change (store, .by(.adding(.tag( t ))) )
            respond(.tag(t, .wasAdded)             )
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let store: Store
    private let respond: (Response) -> ()
}

extension AddTagUseCase.Response: Equatable {}
