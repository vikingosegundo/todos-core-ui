//
//  TagTodoItemUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 16/09/2020.
//

import ModelState

func request(_ request: TagTodoItemUseCase.Request, from tagger: TagTodoItemUseCase) { tagger.request(request) }

struct TagTodoItemUseCase: UseCase {
    enum Request {
        case tag  (TodoItem, with: Tag)
        case untag(TodoItem, from: Tag)
    }
    enum Response {
        case item( TodoItem, _TodoItem)
        
        enum _TodoItem {
            case wasTagged  (with: Tag)
            case wasUntagged(from: Tag)        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .tag(let item, with: let tag):
            func removingTag(_ t: Tag) -> Bool { t.id != tag.id }
            let tagListWithoutTag = createArray(by: removingTag, from: tags(in: state(of: store)))
            let updatedTag = change(tag, .by(.adding(item)))
            
            change (store, .by(.replacing( .tags(with: tagListWithoutTag + [ updatedTag ]) )) )
            respond(.item(item, .wasTagged(with: tag))                                        )

        case .untag(let item, from: let tag):
            func removingTag(_ t: Tag) -> Bool { t.id != tag.id }
            let tagListWithoutTag = createArray(by: removingTag, from: tags(in: state(of: store)))
            let updatedTag = change(tag, .by(.removing(item)))

            change (store, .by(.replacing(.tags( with: tagListWithoutTag + [ updatedTag ]))) )
            respond(.item(item, .wasUntagged(from: tag))                                     )
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let store: Store
    private let respond: (Response) -> ()
}
