//
//  TaggingFeature.swift
//  Todos
//
//  Created by Manuel Meyer on 14/09/2020.
//

import ModelState

func createTaggingFeature(store: Store, output: @escaping Output) -> Input {
    let tagAdder = AddTagUseCase(store: store) { response in
        switch response {
        case .tag(let tag, .wasAdded):
            output( .tagging(.response(.wasAdded(tag)))                          )
            output( .journal(.add(Entry(text: "tag \"\(tag.name)\" was added"))) )
        }
    }
    let itemTagger = TagTodoItemUseCase(store: store) { response in
        switch response {
        case let .item(item, .wasTagged(with: tag)):
            output( .tagging(.response(.wasTagged(item, with: tag)))                       )
            output( .journal(.add(Entry(text: "\(item.text) was tagged \"\(tag.name)\""))) )
        case let .item(item, .wasUntagged(from: tag)):
            output( .tagging(.response(.wasUntagged(item, from: tag)))                       )
            output( .journal(.add(Entry(text: "\(item.text) was untagged \"\(tag.name)\""))) )
        }
    }
    return { msg in
        if case .tagging(  .add(let tag))                  = msg { request(   .add(tag: tag       ), from: tagAdder   ) }
        if case .tagging(  .tag(let item, with: let tag )) = msg { request(   .tag(item, with: tag), from: itemTagger ) }
        if case .tagging(.untag(let item, from: let tag )) = msg { request( .untag(item, from: tag), from: itemTagger ) }
    }
}
