//
//  AddTodoItemUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 13/09/2020.
//

import ModelState

func request(_ request: AddTodoItemUseCase.Request, from adder: AddTodoItemUseCase) { adder.request(request) }

struct AddTodoItemUseCase: UseCase {
    enum Request  { case adding(item: TodoItem) }
    enum Response {
        case item(TodoItem, _Item)
        enum _Item {
            case wasAdded
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.interactor = Interactor(store: store, responder: responder)
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .adding(item: let item): interactor.add(item: item)
        }
    }
    
    typealias RequestType  = Request
    typealias ResponseType = Response
    
    private let interactor: AddTodoItemUseCase.Interactor
}


private extension AddTodoItemUseCase {
    private final class Interactor {

        init(store: Store, responder: @escaping (Response) -> ()) {
            self.store   = store
            self.respond = responder
        }
        
        func add(item i: TodoItem) {
            change (store, .by(.adding(.item(i))) )
            respond(.item(i, .wasAdded)           )
        }
        
        private let store  : Store
        private let respond: (Response) -> ()
    }
}

extension AddTodoItemUseCase.Response: Equatable { }
