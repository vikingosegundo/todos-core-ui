//
//  CheckTodoItemUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 13/09/2020.
//

import ModelState

func request(_ request: CheckTodoItemUseCase.Request, from checker: CheckTodoItemUseCase) { checker.request(request) }

struct CheckTodoItemUseCase: UseCase {
    enum Request {
        case checking(item: TodoItem)
        case unchecking(item: TodoItem)
    }
    enum Response {
        case item(TodoItem, _Item)
        enum _Item {
            case wasChecked
            case wasUnChecked
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .checking  (item: let item): change(store, .by(.updating(.item( with: change(item, .by(.updating(.completed(to: true ) ) ))) ))); respond( .item(item, .wasChecked  ) )
        case .unchecking(item: let item): change(store, .by(.updating(.item( with: change(item, .by(.updating(.completed(to: false) ) ))) ))); respond( .item(item, .wasUnChecked) )
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let store: Store
    private let respond: (Response) -> ()
}
