//
//  DeleteTodoItemUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 13/09/2020.
//
import ModelState

func request(_ request: DeleteTodoItemUseCase.Request, from remover: DeleteTodoItemUseCase) { remover.request(request) }

struct DeleteTodoItemUseCase: UseCase {
    enum Request  { case deleting    (item: TodoItem) }
    enum Response {
        case item(TodoItem, _Item)
        enum _Item {
            case wasDeleted
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store   = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .deleting(item: let i):
            change (store, .by(.removing(.item(i))) )
            respond(.item(i, .wasDeleted)           )
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let store: Store
    private let respond: (Response) -> ()
}

extension DeleteTodoItemUseCase.Response: Equatable { }
