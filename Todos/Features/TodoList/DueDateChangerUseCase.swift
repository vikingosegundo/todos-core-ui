//
//  DueDateChanger.swift
//  Todos
//
//  Created by Manuel Meyer on 18.04.21.
//

import Foundation.NSDate
import ModelState

func request(_ request: DueDateChangerUseCase.Request, from dueDateChanger: DueDateChangerUseCase) { dueDateChanger.request(request) }

struct DueDateChangerUseCase: UseCase {
    enum Request { case changing(dueDate: Date?, for: TodoItem)}
    enum Response {
        case item(TodoItem, _Item)
        enum _Item {
            case dueDateWasChanged
        }
    }
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store   = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case .changing(dueDate: let d, let item):
            let updated = change( item, .by(.updating(.dueDate(to: d))) )
            change (store, .by(.updating(.item(with: updated)))         )
            respond(.item( updated, .dueDateWasChanged)                 )
        }
    }
    
    private let store  : Store
    private let respond: (Response) -> ()

    typealias RequestType  = Request
    typealias ResponseType = Response
}
