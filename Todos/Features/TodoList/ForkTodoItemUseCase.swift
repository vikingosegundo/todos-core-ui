//
//  ForkTodoItemUseCase.swift
//  Todos
//
//  Created by Manuel Meyer on 13.04.21.
//

import ModelState

func request(_ request: ForkTodoItemUseCase.Request, from forker: ForkTodoItemUseCase) { forker.request(request) }

struct ForkTodoItemUseCase: UseCase {
    enum Request  { case forking  (TodoItem, with: [Fork]) }
    enum Response {
        case item(TodoItem, _Item)
        enum _Item {
            case wasForked
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.interactor = Interactor(store: store, responder: responder)
    }
    
    fileprivate func request(_ request: Request) {
        interactor.request(request)
    }
    
    typealias RequestType  = Request
    typealias ResponseType = Response
    private let interactor: Interactor
}

private extension ForkTodoItemUseCase {
    class Interactor {
        init(store: Store, responder: @escaping (Response) -> ()) {
            self.store = store
            self.respond = responder
        }
        
        func request(_ request: Request) {
            switch request {
            case let .forking(item, changes):
                let updatedItem = change(item, .by(.updating(.fork( changes.map {
                    switch $0 {
                    case .text(let t): return .by(.updating(.text(to: t)))
                    case .due (let d): return .by(.updating(.dueDate(to: d)))
                    }
                }) )))
                change (store, .by(.adding(.item(updatedItem))) )
                respond(.item(updatedItem, .wasForked)          )
            }
        }
        private let store   : Store
        private let respond : (Response) -> ()
    }
}
