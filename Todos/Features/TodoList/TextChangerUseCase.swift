//
//  TextChanger.swift
//  Todos
//
//  Created by Manuel Meyer on 18.04.21.
//
import ModelState

func request(_ request: TextChangerUseCase.Request, from textChanger: TextChangerUseCase) { textChanger.request(request) }

struct TextChangerUseCase: UseCase {
    enum Request { case changing(text: String, for: TodoItem)}
    enum Response {        
        case text(String, _Text)
        enum _Text {
            case wasChanged(for: TodoItem)
        }
    }
    
    init(store: Store, responder: @escaping (Response) -> ()) {
        self.store = store
        self.respond = responder
    }
    
    fileprivate func request(_ request: Request) {
        switch request {
        case let .changing(text: t, for: i):
            let updatedItem = change(i, .by(.updating(.text(to: t))) )
            change (store, .by(.updating(.item( with: updatedItem))) )
            respond(.text(t, .wasChanged(for: updatedItem))          )
        }
    }
    
    private let store: Store
    private let respond: (Response) -> ()

    typealias RequestType = Request
    typealias ResponseType = Response
}
