//
//  TodoListFeature.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

import ModelState

func createTodoListFeature(store: Store, output: @escaping Output) -> Input {
   
    let itemAdder   = AddTodoItemUseCase   (store:store, responder:handle(output:output))
    let itemDeleter = DeleteTodoItemUseCase(store:store, responder:handle(output:output))
    let itemChecker = CheckTodoItemUseCase (store:store, responder:handle(output:output))
    let dueChanger  = DueDateChangerUseCase(store:store, responder:handle(output:output))
    let textChanger = TextChangerUseCase   (store:store, responder:handle(output:output))
    let forker      = ForkTodoItemUseCase  (store:store, responder:handle(output:output))
    
    return { msg in
        if case let .todo(.add    (item)             ) = msg { request( .adding    (item:item)         , from:itemAdder   ) }
        if case let .todo(.delete (item)             ) = msg { request( .deleting  (item:item)         , from:itemDeleter ) }
        if case let .todo(.check  (item)             ) = msg { request( .checking  (item:item)         , from:itemChecker ) }
        if case let .todo(.uncheck(item)             ) = msg { request( .unchecking(item:item)         , from:itemChecker ) }
        if case let .todo(.change (.due(d, item))    ) = msg { request( .changing  (dueDate:d,for:item), from:dueChanger  ) }
        if case let .todo(.change (.text(text, item))) = msg { request( .changing  (text:text,for:item), from:textChanger ) }
        if case let .todo(.fork   (item, with: chngs)) = msg { request( .forking   (item,with:chngs)   , from:forker      ) }
    }
}

private func handle(output: @escaping Output) -> (DueDateChangerUseCase.Response) -> () {
    return { response in
        switch response {
        case let .item(i, .dueDateWasChanged): output( .todo(.response(.dueDateWasChanged(i))) )
        }
    }
}

private func handle(output: @escaping Output) -> (TextChangerUseCase.Response) -> () {
    return { response in
        switch response {
        case let .text(_, .wasChanged(for: item)): output(.todo(.response(.dueDateWasChanged(item))))
        }
    }
}

private func handle(output: @escaping Output) -> (AddTodoItemUseCase.Response) -> () {
    return { response in
        switch response {
        case let .item(item, .wasAdded):
            output( .todo(.response(.wasAdded(item)))                    )
            output( .journal(.add(Entry(text:"\(item.text) was added"))) )
        }
    }
}

private func handle(output: @escaping Output) -> (DeleteTodoItemUseCase.Response) -> () {
    return { response in
        switch response {
        case let .item(item, .wasDeleted):
            output( .todo(.response(.wasDeleted(item)))                    )
            output( .journal(.add(Entry(text:"\(item.text) was deleted"))) )
        }
    }
}

private func handle(output: @escaping Output) -> (CheckTodoItemUseCase.Response) -> () {
    return { response in
        switch response {
        case let .item(item, .wasChecked):
            output( .todo(.response(.wasChecked(item)))                      )
            output( .journal(.add(Entry(text:"\(item.text) was completed"))) )
        case let .item(item, .wasUnChecked):
            output( .todo(.response(.wasUnchecked(item)))                    )
            output( .journal(.add(Entry(text:"\(item.text) was uncompleted"))) )
        }
    }
}

private func handle(output: @escaping Output) -> (ForkTodoItemUseCase.Response) -> () {
    return { response in
        switch response {
        case let .item(item, .wasForked):
            output( .todo(.response(.wasForked(item)))                    )
            output( .journal(.add(Entry(text:"\(item.text) was forked"))) )
        }
    }
}
