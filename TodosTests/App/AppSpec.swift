//
//  AppSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Quick
import Nimble

@testable import Todos
import ModelState

class AppSpec: QuickSpec {
    override func spec() {
        describe("App") {
            var app: Input!
            var store: Store!
            var viewState: ViewState!

            let item0 = TodoItem(text: "Hey Ho")
            let item1 = TodoItem(text: "lets go")
            let tag0 = Tag(name: "t0")
            let tag1 = Tag(name: "t1")

            let storefile = "apptests.json"
            beforeEach {
                store = createDiskStore(pathInDocs: storefile)
                viewState = ViewState(store: store)
                app = createAppDomain(store: store, receivers: [viewState.handle(msg: )], rootHandler: { app($0) })
            }
            afterEach {
                app = nil
                viewState = nil
                destroy(&store)
            }

            context("items") {
                context("add items") {
                    beforeEach {
                        app(.todo(.add(item0)))
                        app(.todo(.add(item1)))
                    }
                    it("will add them to store") { expect(items(in: viewState)).to(contain([item0, item1])) }
                }
                context("remove items") {
                    beforeEach {
                        app(.todo(.add(item0)))
                        app(.todo(.add(item1)))
                        app(.todo(.delete(item0)))
                    }
                    it("will be remove from store") { expect(items(in: viewState)).toNot(contain(item0)) }
                }
                context("checking") {
                    beforeEach {
                        app(.todo(.add(item0)))
                        app(.todo(.add(item1)))
                        app(.todo(.check(item0)))
                    }
                    context("item") {
                        it("item0 was checked"  ) {expect(items(in: viewState).first(where: { $0.id == item0.id })!.completed) == true  }
                        it("item1 wasnt checked") {expect(items(in: viewState).first(where: { $0.id == item1.id })!.completed) == false }
                    }
                }
                context("unchecking") {
                    beforeEach {
                        app(.todo(.add( change(item0, .by(.updating(.completed(to: true)))) )))
                        app(.todo(.add( change(item1, .by(.updating(.completed(to: true)))) )))
                        app(.todo(.uncheck(item0)))
                    }
                    context("item") {
                        it("item0 wasnt unchecked") {expect(items(in: viewState).first(where: { $0.id == item0.id })!.completed) == false }
                        it("item1 was unchecked"  ) {expect(items(in: viewState).first(where: { $0.id == item1.id })!.completed) == true  }
                    }
                }
            }
            context("tags") {
                context("add tags") {
                    beforeEach {
                        app(.tagging(.add(tag0)))
                        app(.tagging(.add(tag1)))
                    }
                    it("will update the view state") {
                        expect(tags(in:viewState)).to(contain([tag0, tag1]))
                    }
                }
                context("tagging items") {
                    beforeEach {
                        app(.todo(.add(item0)))
                        app(.todo(.add(item1)))
                        app(.tagging(.add(tag0)))
                        app(.tagging(.add(tag1)))
                        app(.tagging(.tag(item1, with: tag0)))
                        app(.tagging(.tag(item0, with: tag1)))
                    }
                    context("updating view state") {
                        it("tags item1 with first tag"      ) { expect(first(of: tags(in:viewState))?.tagged).to   (contain([item1.id])) }
                        it("doesnt tag item0 with first tag") { expect(first(of: tags(in:viewState))?.tagged).toNot(contain([item0.id])) }
                        it("tags item0 with last tag"       ) { expect( last(of: tags(in:viewState))?.tagged).to   (contain([item0.id])) }
                        it("doesnt tag item1 with last tag" ) { expect( last(of: tags(in:viewState))?.tagged).toNot(contain([item1.id])) }
                    }
                }
                context("untagging") {
                    beforeEach {
                        app(.todo(.add(item0)))
                        app(.todo(.add(item1)))
                        app(.tagging(.add(tag0)))
                        app(.tagging(.add(tag1)))
                        app(.tagging(.tag(item1, with: tag0)))
                        app(.tagging(.tag(item0, with: tag1)))
                        app(.tagging(.untag(item1, from: tag0)))
                        app(.tagging(.untag(item0, from: tag1)))
                    }
                    context("items") {
                        it("first tag isnt used") { expect(first(of: tags(in:viewState))?.tagged).to(beEmpty()) }
                        it("last tag isnt used")  { expect( last(of: tags(in:viewState))?.tagged).to(beEmpty()) }
                    }
                }
            }
        }
    }
}
