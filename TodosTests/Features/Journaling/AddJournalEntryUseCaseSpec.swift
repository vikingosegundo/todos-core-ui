//
//  AddJournalEntryUseCaseSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class AddJournalEntryUseCaseSpec: QuickSpec {
    override func spec() {
        describe("AddJournalEntryUseCase") {
            var entryAdder: AddJournalEntryUseCase!
            var store: Store!
            var responses: [AddJournalEntryUseCase.Response]!

            let storefile = "AddJournalEntryUseCase.json"
            beforeEach {
                responses = []
                store = createDiskStore(pathInDocs: storefile)
                entryAdder = AddJournalEntryUseCase(store: store) {
                    responses.append($0)
                }
            }
            afterEach {
                responses = nil
                destroy(&store)
                entryAdder = nil
            }

            let entry = Entry(text: "entry")
            context("add entry"){
                beforeEach {
                    request(.add(entry: entry), from:entryAdder)
                }
                it("will change the state") {
                    let lastEntry = last(of: entries(in: state(of: store)))
                    expect(lastEntry).to(equal(entry))
                }
                it("emit entry wasAdded reponse"){
                    expect(responses) == [.entry(entry, .wasAdded)]
                }
            }
        }
    }
}
