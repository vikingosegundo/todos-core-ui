//
//  JournalingFeature.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class JournalingFeatureSpec: QuickSpec {

    override func spec() {
        describe("JournalingFeature") {
            var journaling: Input!
            var store: Store!
            var messages: [Message]!

            let storefile = "JournalingFeatureSpec.json"
            beforeEach {
                messages = []
                store = createDiskStore(pathInDocs: storefile)
                journaling = createJournalingFeature(store: store) {
                    messages.append($0)
                }
            }
            afterEach {
                journaling = nil
                destroy(&store)
                messages = nil
            }
            
            let entry = Entry(text: "entry")
            context("adding entry") {
                beforeEach {
                    journaling(.journal(.add(entry)))
                }
                it("adds it to state") {
                    expect(entries(in: state(of: store))) == [entry]
                }
                it("emits entry wasAdded msg") {
                    expect(messages).to(contain(Message.journal(.response(.wasAdded(entry)))))
                }
            }
        }
    }
}
