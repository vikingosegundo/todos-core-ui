//
//  AddTagUseCaseSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Quick
import Nimble

@testable import Todos
import ModelState

class AddTagUseCaseSpec: QuickSpec {
    override func spec() {
        describe("AddTagUseCase"){
            var tagAdder: AddTagUseCase!
            var store: Store!
            var responses: [AddTagUseCase.Response]!

            let storefile = "AddTagUseCase.json"
            beforeEach {
                store = createDiskStore(pathInDocs: storefile)
                responses = []
                tagAdder = AddTagUseCase(store: store){ responses.append($0) }
            }
            afterEach {
                responses = nil
                destroy(&store)
                tagAdder = nil
            }
            
            let t = Tag(name: "Tag 1")
            context("adding tag") {
                beforeEach { request(.add(tag: t), from: tagAdder) }
                it("will add it to state"   ) { expect(last(of: tags(in: state(of: store)))).to(equal( t ))                  }
                it("emits response wasAdded") { expect(last(of: responses                 )).to(equal( .tag(t, .wasAdded) )) }
            }
        }
    }
}
