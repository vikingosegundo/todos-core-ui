//
//  TagSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Quick
import Nimble

@testable import Todos
import ModelState

class TagSpec: QuickSpec {
    override func spec() {
        describe("Tag") {
            let tag  = Tag(name: "tag 1")
            let item = TodoItem(text: "Hello Text")
            let item1 = TodoItem(text: "Hello again")

            context("tagging") {
                let tag = change(tag, .by(.adding(item)), .by(.adding(item1)) )
                it("add item to tag") {
                    expect(tag.tagged) == [item.id, item1.id]
                }
            }
            context("name") {
                it("is the selection title") {
                    expect(tag.selectionTitle) == tag.name
                }
            }
            context("untagging") {
                var appState: AppState!
                beforeEach {
                    appState = change(AppState(),  .by(.adding(.item(item))), .by(.adding(.tag( change(tag, .by(.adding(item) )) ))))
                }
                afterEach {
                    appState = nil
                }
                context("removing item from state") {
                    beforeEach {
                        appState = change(appState, .by(.removing(.item(item))) )
                    }
                    it("also removes it from tags") {
                        expect(first(of: items(in: appState))).to(beNil())
                        expect(first(of: tags (in: appState))).to(equal(tag))
                        expect(first(of: tags (in: appState))?.tagged).to(beEmpty())
                    }
                }
            }
        }
    }
}
