//
//  TagTodoItemUseCaseSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Quick
import Nimble

import ModelState
@testable import Todos

class TagTodoItemUseCaseSpec: QuickSpec {
    override func spec() {
        describe("TagTodoItemUseCase"){
            var itemTagger: TagTodoItemUseCase!
            var store     : Store!
            var responses : [TagTodoItemUseCase.Response]!

            let storefile = "TagTodoItemUseCaseSpec.json"
            beforeEach {
                responses  = []
                store      = createDiskStore(pathInDocs: storefile)
                itemTagger = TagTodoItemUseCase(store: store){ responses.append($0) }
            }
            afterEach {
                itemTagger = nil
                destroy(&store)
                responses = nil
            }

            let item = TodoItem(text: "Hey Ho")
            let tag = Tag(name: "Tag 1")
            beforeEach {
                change(store, .by(.adding(.item(item))) )
                change(store, .by(.adding(.tag(tag)))   )
                request(.tag(item, with: tag), from: itemTagger)
            }
            context("tagging"){
                it("will add item's id to tag's list") { expect(last(of: tags(in: state(of: store)))?.tagged).to(equal( [ item.id ] )) }
            }
            context("untagging"){
                beforeEach { request(.untag(item, from: tag), from: itemTagger) }
                it("will add item's id to tag's list") { expect(last(of: tags(in: state(of: store)))?.tagged).to(equal( [ ] )) }
            }
        }
    }
}
