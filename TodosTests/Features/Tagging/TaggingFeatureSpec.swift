//
//  TaggingFeatureSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class TaggingFeatureSpec: QuickSpec {
    override func spec() {
        describe("TaggingFeature") {
            var tagging: Input!
            var store: Store!
            var messages: [Message]!

            let storefile = "TaggingFeatureSpec.json"
            beforeEach {
                messages = []
                store = createDiskStore(pathInDocs: storefile)
                tagging = createTaggingFeature(store: store) { messages.append($0) }
            }
            afterEach {
                tagging = nil
                destroy(&store)
                messages = nil
            }

            let t = Tag(name: "Tag 1")
            let i = TodoItem(text: "Hey Ho")
            context("adding tag") {
                beforeEach {
                    tagging(.tagging(.add(t)))
                }
                it("will add it to state") {
                    expect(last(of: tags(in: state(of: store)))) == t
                }
                it("emits tag wasAdded msg") {
                    expect(messages).to(contain( .tagging(.response(.wasAdded(t)))) )
                }
                it("will send add entry message") {
                    expect {
                        messages.first(where: { if case .journal(.add(_)) = $0 { return true }; return false })
                    }.toEventuallyNot(beNil())
                }
            }
            context("tag item") {

                beforeEach {
                    change(store, [.by(.adding(.item(i))), .by(.adding(.tag(t))) ])

                    tagging(.tagging(.tag(i, with: t)))
                }
                it("will change the state") {
                    expect(last(of: tags(in: state(of: store)))?.tagged).to(contain(i.id))
                }
                it("emits item wasTagged msg") {
                    expect(messages).to(contain( .tagging(.response(.wasTagged(i, with: t)))) )
                }
            }
            context("untag item") {
                let t = change(t, .by(.adding(i)))
                beforeEach {
                    change(store, [.by(.adding(.item(i))), .by(.adding(.tag(t))) ])
                    tagging(.tagging(.untag(i, from: t)))
                }
                it("will change the state") {
                    expect(last(of: tags(in: state(of: store)))?.tagged).toNot(contain(i.id))
                }
                it("emits item wasUntagged msg") {
                    expect(messages).to(contain( .tagging(.response(.wasUntagged(i, from: t)))) )
                }
                it("will send add entry message") {
                    expect {
                        messages.first(where: { if case .journal(.add(_)) = $0 { return true }; return false })
                    }.toEventuallyNot(beNil())
                }
            }
        }
    }
}
