//
//  AddTodoItemUseCaseSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class AddTodoItemUseCaseSpec: QuickSpec {
    override func spec() {
        describe("AddTodoItemUseCase"){
            var itemAdder: AddTodoItemUseCase!
            var store: Store!
            var responses: [AddTodoItemUseCase.Response]!

            beforeEach {
                store = createDiskStore(pathInDocs: "AddTodoItemUseCaseSpec.json")
                responses = []
                itemAdder = AddTodoItemUseCase(store: store) { responses.append($0) }
            }
            afterEach {
                itemAdder = nil
                responses = nil
                destroy(&store)
            }

            let t = TodoItem(text: "Hey Ho")
            context("adding an item"){
                beforeEach { request(.adding(item: t), from: itemAdder) }
                it("will change the state"      ) { expect(items(in: state(of: store))).to(equal([t]))                 }
                it("will emit response wasAdded") { expect( last(of: responses)       ).to(equal(.item(t, .wasAdded))) }
            }
        }
    }
}
