//
//  CheckTodoItemUseCaseSpec.swift
//  Todos
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class CheckTodoItemUseCaseSpec: QuickSpec {
    override func spec() {
        describe("CheckTodoItemUseCase") {
            var itemChecker: CheckTodoItemUseCase!
            var store: Store!
            var responses: [CheckTodoItemUseCase.Response]!

            let storefile = "CheckTodoItemUseCaseSpec.json"
            beforeEach {
                store = createDiskStore(pathInDocs: storefile)
                responses = []
                itemChecker = CheckTodoItemUseCase(store: store) { responses.append($0) }
            }
            afterEach {
                itemChecker = nil
                responses = nil
                destroy(&store)
            }

            let t = TodoItem(text: "Hey Ho")
            context("check item"){
                beforeEach {
                    change(store, .by(.adding(.item(t))))
                }
                it("check unchecked item") {
                    request(.checking(item: t), from: itemChecker)
                    expect(last(of: items(in: state(of: store)))?.completed) == true
                }
                context("check checked item") {
                    let t = change(t, .by(.updating(.completed(to: true))))
                    beforeEach {
                        request(.checking(item: t), from: itemChecker)
                    }
                    it("doesnt alter the alteration date") {
                        expect(last(of: items(in: state(of: store)))?.text ) == t.text
                        expect(last(of: items(in: state(of: store)))?.completed   ) == t.completed
                        expect(last(of: items(in: state(of: store)))?.creationDate) == t.creationDate
                        expect(last(of: items(in: state(of: store)))?.alterDate   ) == t.alterDate
                    }
                }
            }
            context("uncheck item"){
                let t = change(t, .by(.updating(.completed(to: true))))
                beforeEach {
                    change(store, .by(.adding(.item(t))) )
                }
                it("state is setup correctly") {
                    expect(last(of: items(in: state(of: store)))?.completed) == true
                }
                it("uncheck checked item") {
                    request(.unchecking(item: t), from: itemChecker)
                    expect(last(of: items(in: state(of: store)))?.completed) == false
                }
                context("uncheck unchecked item") {
                    let t = change(t, .by(.updating(.completed(to: false))))
                    beforeEach {
                        request(.unchecking(item: t), from: itemChecker)
                    }
                    it("doesnt alter the alteration date") {
                        expect(last(of: items(in: state(of: store)))?.text)         == t.text
                        expect(last(of: items(in: state(of: store)))?.completed)    == t.completed
                        expect(last(of: items(in: state(of: store)))?.creationDate) == t.creationDate
                        expect(last(of: items(in: state(of: store)))?.alterDate)    == t.alterDate
                    }
                }
            }
        }
    }
}
