//
//  DeleteTodoItemUseCaseSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class DeleteTodoItemUseCaseSpec: QuickSpec {
    override func spec() {
        describe("DeleteTodoItemUseCase") {
            var itemDeleter: DeleteTodoItemUseCase!
            var store: Store!
            var responses: [DeleteTodoItemUseCase.Response]!

            let storefile = "DeleteTodoItemUseCaseSpec.json"
            beforeEach {
                store = createDiskStore(pathInDocs: storefile)
                responses = []
                itemDeleter = DeleteTodoItemUseCase(store: store) { responses.append($0) }
            }
            afterEach {
                itemDeleter = nil
                responses = nil
                destroy(&store)
            }
            
            let t = TodoItem(text: "Hey Ho")

            beforeEach { change(store, .by(.adding(.item(t))) ) }
            context("deleting an item") {
                beforeEach { request(.deleting(item: t), from: itemDeleter) }
                it("will remove it from state") { expect(items(in: state(of: store))).to(equal(  []                   )) }
                it("emits item wasDeleted rsp") { expect( last(of: responses)       ).to(equal( .item(t, .wasDeleted) )) }
            }
        }
    }
}
