//
//  TodoListFeatureSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Quick
import Nimble

@testable import Todos
import ModelState

class TodoListFeatureSpec: QuickSpec {
    override func spec() {
        describe("TodoListFeature") {
            var todoList: Input!
            var store: Store!
            var messages: [Message]!

            let storefile = "TodoListFeatureSpec.json"
            beforeEach {
                store = createDiskStore(pathInDocs: storefile)
                messages = []
                todoList = createTodoListFeature(store: store) { msg in
                    messages.append(msg)
                }
            }
            afterEach {
                todoList = nil
                messages = nil
                destroy(&store)
            }

            let t = TodoItem(text: "Hey Ho")
            context("add item"){
                beforeEach { todoList(.todo(.add(t))) }
                it("will be added to state"         ) { expect(items(in: state(of: store))) == [t]                               }
                it("will emit message wasAdded item") { expect(messages).toEventually(contain( .todo(.response(.wasAdded(t))) )) }
                it("will send add entry message"    ) {
                    expect {
                        messages.first(where: { if case .journal(.add(_)) = $0 { return true }; return false })
                    }.toEventuallyNot(beNil())
                }
            }
            context("delete item") {
                beforeEach { todoList(.todo(.add(t)))   }
                beforeEach {todoList(.todo(.delete(t))) }
                it("will remove item from state"      ) { expect(items(in: state(of: store))) == []                                  }
                it("will emit message wasDeleted item") { expect(messages).toEventually(contain( .todo(.response(.wasDeleted(t))) )) }
                context("will send") {
                    it("add entry message") {
                        expect {
                            messages.first(where: { if case .journal(.add(_)) = $0 { return true }; return false })
                        }.toEventuallyNot(beNil())
                    }
                }
            }
            context("check item") {
                beforeEach { todoList(.todo(.add(t)))   }
                beforeEach { todoList(.todo(.check(t))) }
                it("will change in state"             ) { expect(last(of: items(in: state(of: store)))?.completed) == true                 }
                it("will emit message wasChecked item") { expect(messages).toEventually(contain( Message.todo(.response(.wasChecked(t))))) }
                it("will send add entry message") {
                    expect {
                        messages.first(where: { if case .journal(.add(_)) = $0 { return true }; return false })
                    }.toEventuallyNot(beNil())
                }
            }
            context("uncheck item") {
                beforeEach { todoList(.todo(.add(change(t,  .by(.updating(.completed(to: true))))))) }
                beforeEach { todoList(.todo(.uncheck(t)))                                            }
                it("will change in state"               ) { expect(last(of: items(in: state(of: store)))?.completed) == false                  }
                it("will emit message wasUnchecked item") { expect(messages).toEventually(contain( Message.todo(.response(.wasUnchecked(t))))) }
                it("will send add entry message") {
                    expect {
                        messages.first(where: { if case .journal(.add(_)) = $0 { return true }; return false })
                    }.toEventuallyNot(beNil())
                }
            }
        }
    }
}
