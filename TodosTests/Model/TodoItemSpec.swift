//
//  TodoItemSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 02/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class TodoItemSpec : QuickSpec {
    override func spec() {
        describe("TodoItem") {
            let orig = TodoItem(text:"Hey Ho")
            context("creation default") {
                let now = Date()
                it("has custom text"          ) { expect( orig.text         ) == "Hey Ho"        }
                it("has random id"            ) { expect( orig.id           ).toNot(beNil())     }
                it("is not completed"         ) { expect( orig.completed    ) == false           }
                it("has the creation date set") { expect( orig.creationDate ).to(beCloseTo(now)) }
                it("has no due date set"      ) { expect( orig.dueDate      ).to(beNil())        }
                it("has no alter date set"    ) { expect( orig.alterDate    ).to(beNil())        }
            }
            context("altering") {
                let new = change(orig, .by(.updating(.completed(to: true))))
                let now = Date()
                context("check item") {
                    it("changes completed"           ) { expect( new.completed    ) != orig.completed    }
                    it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                    it("doesnt changes the text"     ) { expect( new.text         ) == orig.text         }
                    it("changes the alterDate"       ) { expect( new.alterDate    ).to(beCloseTo(now))   }
                    it("doesn't change the due date" ) { expect( new.dueDate      ).to(beNil())          }
                    it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }

                    context("double check") {
                        let orig = new
                        let new = change(orig, .by(.updating(.completed(to: true))))
                        it("doesnt change completed"     ) { expect( new.completed    ) == orig.completed    }
                        it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                        it("doesnt changes the text"     ) { expect( new.text         ) == orig.text         }
                        it("doesnt change the alterDate" ) { expect( new.alterDate    ) == orig.alterDate    }
                        it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }
                        it("doesn't change the due date" ) { expect( new.dueDate      ).to(beNil())          }
                    }
                }
                context("uncheck") {
                    let orig = new
                    let new = change(orig, .by(.updating(.completed(to: false))))
                    it("changes completed"           ) { expect( new.completed    ) != orig.completed    }
                    it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                    it("doesnt changes the text"     ) { expect( new.text         ) == orig.text         }
                    it("changes the alterDate"       ) { expect( new.alterDate    ) >  orig.alterDate!   }
                    it("doesn't change the due date" ) { expect( new.dueDate      ).to (beNil())         }
                    it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }

                    context("double uncheck") {
                        let orig = new
                        let new = change(orig, .by(.updating(.completed(to: false))))
                        it("doesnt change completed"     ) { expect( new.completed    ) == orig.completed    }
                        it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                        it("doesnt changes the text"     ) { expect( new.text         ) == orig.text         }
                        it("doesnt change the alterDate" ) { expect( new.alterDate    ) == orig.alterDate    }
                        it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }
                        it("doesn't change the due date" ) { expect( new.dueDate      ).to(beNil())          }
                    }
                }
                context("change text") {
                    let new = change(orig, .by(.updating(.text(to: "Let's Go"))))
                    it("changes the text"            ) { expect( new.text         ) != orig.text         }
                    it("doesnt change completed"     ) { expect( new.completed    ) == orig.completed    }
                    it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                    it("changes the alterDate"       ) { expect( new.alterDate    ).toNot(beNil())       }
                    it("doesn't change the due date" ) { expect( new.dueDate      ).to   (beNil())       }
                    it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }

                    context("double change") {
                        let orig = new
                        let new = change(orig, .by(.updating(.text(to: "Let's Go"))))
                        it("doesnt change completed"     ) { expect( new.completed    ) == orig.completed    }
                        it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                        it("doesnt changes the text"     ) { expect( new.text         ) == orig.text         }
                        it("doesnt change the alterDate" ) { expect( new.alterDate    ) == orig.alterDate    }
                        it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }
                        it("doesn't change the due date" ) { expect( new.dueDate      ).to(beNil())          }
                    }
                }
                context("due date") {
                    let new = change(orig, .by(.updating(.dueDate(to: Date.tomorrow.noon))))
                    it("doesn't change the due date" ) { expect( new.dueDate      ).toNot(beNil())       }
                    it("doesnt change the text"      ) { expect( new.text         ) == orig.text         }
                    it("doesnt change completed"     ) { expect( new.completed    ) == orig.completed    }
                    it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                    it("changes the alterDate"       ) { expect( new.alterDate    ).toNot(beNil())       }
                    it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }

                    context("double change") {
                        let orig = new
                        let new = change(orig, .by(.updating(.dueDate(to: Date.tomorrow.noon))))
                        it("doesn't change the due date" ) { expect( new.dueDate      ) == orig.dueDate      }
                        it("doesnt change completed"     ) { expect( new.completed    ) == orig.completed    }
                        it("doesnt changes the id"       ) { expect( new.id           ) == orig.id           }
                        it("doesnt changes the text"     ) { expect( new.text         ) == orig.text         }
                        it("doesnt change the alterDate" ) { expect( new.alterDate    ) == orig.alterDate    }
                        it("doesn't change creation date") { expect( new.creationDate ) == orig.creationDate }
                    }
                }
            }
            context("forking") {
                let orig = change(orig, .by(.updating(.dueDate(to: Date.tomorrow.noon))), .by(.updating(.completed(to: true))) )

                context("without altering any data") {
                    let fork = change(orig, .by(.updating(.fork([]))))
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("doesn't change the alterDate") { expect( fork.alterDate    ) == orig.alterDate    }
                    it("doesn't change the text"     ) { expect( fork.text         ) == orig.text         }
                    it("doesn't change completed"    ) { expect( fork.completed    ) == orig.completed    }
                    it("doesn't change the due date" ) { expect( fork.dueDate      ) == orig.dueDate      }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
                context("with altering text") {
                    let fork = change(orig, .by(.updating(.fork([.by(.updating(.text(to: "Let's go")))]))))

                    it("sets the text to given value") { expect( fork.text         ) == "Let's go"        }
                    it("changes the text"            ) { expect( fork.text         ) != orig.text         }
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("changes the alterDate"       ) { expect( fork.alterDate    ) != orig.alterDate    }
                    it("doesn't change the due date" ) { expect( fork.dueDate      ) == orig.dueDate      }
                    it("doesn't change completed"    ) { expect( fork.completed    ) == orig.completed    }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
                context("altering completed") {
                    let fork = change(orig, .by(.updating(.fork([.by(.updating(.completed(to: !orig.completed)))]))))

                    it("changes completed"           ) { expect( fork.completed    ) != orig.completed    }
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("changes the alterDate"       ) { expect( fork.alterDate    ) != orig.alterDate    }
                    it("doesn't change the text"     ) { expect( fork.text         ) == orig.text         }
                    it("doesn't change the due date" ) { expect( fork.dueDate      ) == orig.dueDate      }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
                context("altering duedate") {
                    let newDueDate = orig.dueDate?.dayAfter
                    let fork = change(orig, .by(.updating(.fork([.by(.updating(.dueDate(to: newDueDate)))]))))

                    it("sets dueDate to given value" ) { expect( fork.dueDate      ) == newDueDate        }
                    it("changes the due date"        ) { expect( fork.dueDate      ) != orig.dueDate      }
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("changes the alterDate"       ) { expect( fork.alterDate    ) != orig.alterDate    }
                    it("doesn't change the text"     ) { expect( fork.text         ) == orig.text         }
                    it("doesn't change completed"    ) { expect( fork.completed    ) == orig.completed    }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
                context("altering duedate and completed") {
                    let newDueDate = orig.dueDate?.dayAfter
                    let fork = change(orig, .by(.updating(.fork([.by(.updating(.dueDate(to: newDueDate))), .by(.updating(.completed(to: !orig.completed)))]))))
                    it("changes the due date"        ) { expect( fork.dueDate      ) != orig.dueDate      }
                    it("sets dueDate to given value" ) { expect( fork.dueDate      ) == newDueDate        }
                    it("changes completed"           ) { expect( fork.completed    ) != orig.completed    }
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("changes the alterDate"       ) { expect( fork.alterDate    ) != orig.alterDate    }
                    it("doesn't change the text"     ) { expect( fork.text         ) == orig.text         }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
                context("altering text and duedate") {
                    let newDueDate = orig.dueDate?.dayAfter
                    let fork = change(orig, .by(.updating(.fork([.by(.updating(.dueDate(to: newDueDate))), .by(.updating(.text(to: "Let's go")))]))))
                    it("sets the text to given value") { expect( fork.text         ) == "Let's go"        }
                    it("changes the text"            ) { expect( fork.text         ) != orig.text         }
                    it("sets dueDate to given value" ) { expect( fork.dueDate      ) == newDueDate        }
                    it("changes the due date"        ) { expect( fork.dueDate      ) != orig.dueDate      }
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("changes the alterDate"       ) { expect( fork.alterDate    ) != orig.alterDate    }
                    it("doesn't change completed"    ) { expect( fork.completed    ) == orig.completed    }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
                context("altering text, duedate and completed") {
                    let newDueDate = orig.dueDate?.dayAfter
                    let fork = change(orig, .by(.updating(.fork([.by(.updating(.dueDate(to: newDueDate))), .by(.updating(.text(to: "Let's go"))), .by(.updating(.completed(to: !orig.completed)))]))))
                    it("sets the text to given value") { expect( fork.text         ) == "Let's go"        }
                    it("changes the text"            ) { expect( fork.text         ) != orig.text         }
                    it("sets dueDate to given value" ) { expect( fork.dueDate      ) == newDueDate        }
                    it("changes the due date"        ) { expect( fork.dueDate      ) != orig.dueDate      }
                    it("changes completed"           ) { expect( fork.completed    ) != orig.completed    }
                    it("changes the id"              ) { expect( fork.id           ) != orig.id           }
                    it("changes the alterDate"       ) { expect( fork.alterDate    ) != orig.alterDate    }
                    it("doesn't change creation date") { expect( fork.creationDate ) == orig.creationDate }
                }
            }
        }
    }
}
