//
//  AppStateSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 18/09/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class AppStateSpec : QuickSpec {
      override func spec() {
        describe("AppState") {
            let appState = AppState()

            let item = TodoItem(text: "Hello Text")
            context("creating") {
                it("with defaults") {
                    expect(items  (in: appState)).to(beEmpty())
                    expect(tags   (in: appState)).to(beEmpty())
                    expect(entries(in: appState)).to(beEmpty())
                }
            }
            context("adding") {
                context("an item") {
                    let appState = change(appState, .by(.adding(.item (TodoItem(text: "Hello Text")))))
                    it("changes the appstate items count to 1")  {
                    expect(items(in: appState)).to(haveCount(1)) }
                }
                context("an entry") {
                    let appState = change(appState, .by(.adding(.entry(Entry(text: "hello")))))
                    it("changes the appstate entries count to 1")  {
                    expect(entries(in: appState)).to(haveCount(1)) }
                }
                context("an tag") {
                    let appState = change(appState, .by(.adding(.tag(Tag(name: "hello")))))
                    it("changes the appstate tags count to 1")  {
                    expect(tags(in: appState)).to(haveCount(1)) }
                }
            }
            context("updating item") {
                let appState = change(appState, .by(.adding(.item(item))))
                context("by completing") {
                    let updatedItem = change(item,     .by(.updating(.completed(to: true)))     )
                    let appState    = change(appState, .by(.updating(.item(with: updatedItem))) )
                    it("changes the first items completed to true") {
                        expect(first(of: items(in: appState))?.completed).to(beTrue())
                    }
                }
                context("by changing due date") {
                    let updatedItem = change(item,     .by(.updating(.dueDate(to: Date.tomorrow.noon))) )
                    let appState    = change(appState, .by(.updating(.item(with: updatedItem)))         )
                    it("changes the first items due date to tomorrow noon"){
                        expect(dueDate(of:first(of: items(in: appState))!)).to(equal(Date.tomorrow.noon))
                    }
                    it("changes the first items due date to tomorrow noon"){
                        expect(dueDate(of:first(of:  appState.items)!)).to(equal(Date.tomorrow.noon))
                    }
                }
            }
            context("removing item") {
                let appState = change(appState, .by(.adding(.item(item))))
                context("by removing it from state") {
                    let appState = change(appState, .by(.removing(.item(item))))
                    it("changes items in state to be empty") {
                        expect(items(in: appState)).to(beEmpty())
                    }
                }
            }
        }
    }
}
