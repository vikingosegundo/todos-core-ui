//
//  StateStoreSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class StateStoreSpec : QuickSpec {
    override func spec() {
        describe("StateStore") {
            var store: Store!

            let item  = TodoItem(text: "Wie wenn da einer und er hielte")
            let tag   = Tag     (name: "Wie wenn da einer und er hielte")
            let entry = Entry   (text: "Wie wenn da einer und er hielte")
            
            let updatedItem = change(item, .by(.updating(.text(to: "Ein frühgereiftes Kind das schielte"))))

            let t1 = Tag(name: "t1")
            let t2 = Tag(name: "t2")
            let t3 = Tag(name: "t3")
            let t4 = Tag(name: "t4")
            
            beforeEach { store = createDiskStore(pathInDocs: "StateStoreSpec.json") }
            afterEach  { destroy(&store) }

            context("items") {
                context("add item") {
                    beforeEach {
                        change(store, .by(.adding(.item(item))))
                    }
                    it("will be added to store's state items") {
                        expect(items(in: state(of: store))).to(equal( [item] ))
                    }
                }
                context("update item") {
                    beforeEach {
                        change(store, .by(.adding(.item(item))))
                        change(store, .by(.updating(.item(with: updatedItem))))
                    }
                    it("will update it in store") {
                        expect(items(in: state(of: store))).to   (contain(updatedItem))
                        expect(items(in: state(of: store))).toNot(contain(item       ))
                    }
                }
                context("delete item") {
                    beforeEach {
                        change(store, .by(.adding(.item(item))))
                        change(store, .by(.removing(.item(item))))
                    }
                    it("will remove it from store") {
                        expect(items(in: state(of: store))) == []
                    }
                }
            }
            context("tags"){
                context("add tag") {
                    beforeEach {
                        change(store, .by(.adding(.tag(tag))) )
                    }
                    it("will add it to store's state") {
                        expect(tags(in: state(of: store))) == [tag]
                    }
                }
                context("replace tags") {
                    beforeEach {
                        change(store, .by(.replacing(.tags(with: [t1, t2]))))
                        change(store, .by(.replacing(.tags(with: [t3, t4]))))
                    }
                    it("will replace tags in store") {
                        expect(tags(in: state(of: store))) == [t3, t4]
                    }
                }
            }
            context("journaling") {
                context("add entry") {
                    beforeEach {
                        change(store, .by(.adding(.entry(entry))))
                    }
                    it("will add it to store's state") {
                        expect(entries(in: state(of: store))) == [entry]
                    }
                }
            }
        }
    }
}
