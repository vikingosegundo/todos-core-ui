//
//  ViewStateSpec.swift
//  TodosTests
//
//  Created by Manuel Meyer on 01/10/2020.
//

import Foundation
import Quick
import Nimble

@testable import Todos
import ModelState

class ViewStateSpec : QuickSpec {
    override func spec() {
        describe("ViewState") {
            var viewState: ViewState!
            var store: Store!

            beforeEach {
                store = createDiskStore(pathInDocs: "ViewStateSpec.json")
                viewState = ViewState(store: store, dismissToastAfter: 0.001)
            }
            afterEach {
                viewState = nil
                destroy(&store)
            }

            let tag = Tag(name:"hey ho")
            let item = TodoItem(text:"hey ho")
            let entry = Entry(text:"hey ho")

            context("adding item to store") {
                it("will update the viewstate") {
                    change(store, .by(.adding(.item(item))))
                    expect(items(in: viewState)).to(equal([item]))
                }
            }
            context("adding entry to store") {
                it("will update the viewstate") {
                    change(store, .by(.adding(.entry(entry))))
                    expect(entries(in: viewState)).to(equal([entry]))
                }
            }
            context("adding tag to store") {
                it("will update the viewstate") {
                    change(store, .by(.adding(.tag(tag))))
                    expect(tags(in:viewState)).to(equal([tag]))
                }
            }
            context("will listen for item was deleted msg") {
                it("will keep deleted item") {
                    viewState.handle(msg: .todo(.response(.wasDeleted(item))))
                    expect(deletedItem(in: viewState)) == item
                }
            }
            context("will listen for item was deleted msg") {
                it("will keep deleted item for short time") {
                    viewState.handle(msg: .todo(.response(.wasDeleted(item))))
                    expect(deletedItem(in: viewState)) == item
                    expect(deletedItem(in: viewState)).toEventually(beNil())
                }
            }
        }
    }
}
